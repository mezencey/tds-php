<?php
require_once ('ConnexionBaseDeDonnees.php');
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        $login,
        $nom,
        $prenom,
   ) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login): void
    {
        $this->login = substr($login, 64);;
    }

    /**
     * @return mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom): string
    {
        return $this->prenom = $prenom;
    }


    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() {
        return "<li> Utilisateur " .$this->prenom ." " .$this->nom ." de login " .$this->login ." </li>";
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau["login"], $utilisateurFormatTableau["nom"], $utilisateurFormatTableau["prenom"]);
    }

    public static function getUtilisateur() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateurFormatTableau = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);

        $utilisateurs = array();
        foreach ($utilisateurFormatTableau as $utilisateur) {
            $utilisateurs[] = new Utilisateur($utilisateur["login"], $utilisateur["nom"], $utilisateur["prenom"]);
        }

        return $utilisateurs;
    }

    public static function getUtilisateurParLogin(string $login) : Utilisateur|null {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        return empty($utilisateurFormatTableau) ? null : Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {
        $sql = "INSERT INTO utilisateur(login, nom, prenom) VALUES(:login, :nom, :prenom)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "login" => $this->login,
            "nom" => $this->nom,
            "prenom" => $this->prenom,
        );
        $pdoStatement->execute($values);
    }

}