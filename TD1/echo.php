<?php
$utilisateurs = [
        ["prenom" => "yanhis", "nom" => "Mezence", "login" => "mezencey"],
        ["prenom" => "benjamin", "nom" => "Pouget", "login" => "pougetb"],
    ]
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        <h1>Liste des utilisateurs :</h1>
        <ul>
            <?php
                if(empty($utilisateurs)) {
                    echo "Aucun utilisateur";
                }
                else {
                    foreach ($utilisateurs as $utilisateur) {
                        echo "<li><p> Utilisateur " .$utilisateur["prenom"] ." " .$utilisateur["nom"] ." de login " .$utilisateur["login"] ." </p></li>";
                    }
                }
            ?>
        </ul>
    </body>
</html> 